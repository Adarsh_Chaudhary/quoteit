package com.adarsh.quoteit;

/**
 * Created by Adarsh on 11-08-2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;

public class SendActivity extends AppCompatActivity implements View.OnClickListener {
    EditText et_quote, et_email, et_name;
    String squote, sname, semail;
    String date;
    Button btn_send;
    Button btn_clear;
    LinearLayout ll_send;
    ProgressDialog progressDialog;
    AdView adViewSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);

        et_quote = (EditText) findViewById(R.id.et_quote);
        et_email = (EditText) findViewById(R.id.et_email);
        et_name = (EditText) findViewById(R.id.et_name);
        btn_send = (Button) findViewById(R.id.btn_send);
        btn_clear = (Button) findViewById(R.id.btn_clear);
        ll_send=(LinearLayout)findViewById(R.id.ll_send);
        adViewSend=(AdView)findViewById(R.id.ad_send);
        AdRequest adRequestSend=new AdRequest.Builder().build();
        adViewSend.loadAd(adRequestSend);
        progressDialog = new ProgressDialog(SendActivity.this);
        progressDialog.setMessage("Please wait ....");
        progressDialog.setCancelable(false);

        btn_send.setOnClickListener(this);
        btn_clear.setOnClickListener(this);

        if (!isConnection()) {
            showConnectionErrorDialog();
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_send:
                squote = et_quote.getText().toString();
                semail = et_email.getText().toString();
                sname = et_name.getText().toString();

                if (squote.equals("") || semail.equals("") || sname.equals("")) {
                    Snackbar.make(view, "All fields are required to be filled before sending", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else if (!semail.contains("@")) {
                    Snackbar.make(view, "Enter Valid Email ID", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else {
                    Background background = new Background();
                    background.execute(squote, semail, sname);
                }
                break;
            case R.id.btn_clear:
                et_quote.setText("");
                et_email.setText("");
                et_name.setText("");
                et_email.setHint("Email ID");
                et_name.setHint("Name");
                et_quote.setHint("Write the Quotation");
                break;
        }
    }

    private class Background extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            if (isConnection()) {
                super.onPreExecute();
                progressDialog.show();
            } else {
                showConnectionErrorDialog();
                startActivity(getIntent());
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            squote = strings[0];
            semail = strings[1];
            sname = strings[2];

            try {
                URL url = new URL("http://velapanti.esy.es/quotation/add.php");
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);

                OutputStream OS = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(OS, "UTF-8"));
                String data = URLEncoder.encode("quote", "UTF-8") + "=" + URLEncoder.encode(squote, "UTF-8")
                        + "&" +

                        URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(semail, "UTF-8")
                        + "&" +

                        URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(sname, "UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                OS.close();
                InputStream IS = httpURLConnection.getInputStream();
                IS.close();
                httpURLConnection.disconnect();
                return "Quotation Sent..It will be added after verification...";
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            Snackbar.make(ll_send, result, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            et_quote.setText("");
            et_email.setText("");
            et_name.setText("");
            et_quote.setHint("Write the Quotation");
            et_email.setHint("Email ID");
            et_name.setHint("Name");
        }

    }

    private void showConnectionErrorDialog() {

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle("No Internet !!");
        builder.setMessage("Not connected to Internet Please turn it on and try again");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean isConnection() {
        ConnectivityManager manage = (ConnectivityManager) SendActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manage.getActiveNetworkInfo();
        if (info != null && info.isConnectedOrConnecting()) {

            return true;
        } else {
            return false;
        }
    }
}