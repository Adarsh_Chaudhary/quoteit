package com.adarsh.quoteit;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * Created by Adarsh on 02-10-2016.
 */
public class AboutActivity extends AppCompatActivity {
    AdView ad_about;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        ad_about=(AdView)findViewById(R.id.ad_abt);
        AdRequest adRequestAbt=new AdRequest.Builder().build();
        ad_about.loadAd(adRequestAbt);
    }
}
