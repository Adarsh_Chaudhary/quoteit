package com.adarsh.quoteit.fragment;

/**
 * Created by Adarsh on 29-09-2016.
 */

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


public interface FrndApi {

    /*Retrofit get annotation with our URL
       And our method that will return us the list ob Book
    */
    @GET("/quotation/friends/retrieve.php")
    Call<List<QuoteList>> getJSON();
}
