package com.adarsh.quoteit.fragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


/**
 * Created by Adarsh on 01-10-2016.
 */
public interface MotiveApi {
    @GET("/quotation/motivation/retrieve.php")
    Call<List<QuoteList>> getJSON();
}
