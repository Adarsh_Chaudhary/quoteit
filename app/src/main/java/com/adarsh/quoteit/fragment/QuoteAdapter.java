package com.adarsh.quoteit.fragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adarsh.quoteit.R;

import java.util.List;

/**
 * Created by Adarsh on 29-09-2016.
 */
public class QuoteAdapter extends RecyclerView.Adapter<QuoteAdapter.ViewHolder> {
    List<QuoteList> qItems;
    Context context;


    public QuoteAdapter(Context applicationContext, List<QuoteList> qItems) {
        this.qItems=qItems;
    }

    @Override
    public QuoteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_fetch, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(QuoteAdapter.ViewHolder holder, int position) {
        final QuoteList codeListBeans = qItems.get(position);

        holder.tv_quote.setText(codeListBeans.getQuote());
        holder.tv_name.setText(codeListBeans.getName());
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_name, tv_quote;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_quote = (TextView) itemView.findViewById(R.id.tv_quote);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
        }
    }

    @Override
    public int getItemCount() {
        return qItems.size();
    }
}