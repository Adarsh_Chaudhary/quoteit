package com.adarsh.quoteit.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.adarsh.quoteit.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Adarsh on 29-09-2016.
 */
public class Other extends Fragment {

    private RecyclerView otherRecycler;
    private QuoteAdapter otherAdapter;
    private List<QuoteList> qItems;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View other_view = inflater.inflate(R.layout.fragment_other, container, false);

        otherRecycler = (RecyclerView) other_view.findViewById(R.id.rv_other);
        otherRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        loadJSON();
        return other_view;
    }

    private void loadJSON() {
        qItems = new ArrayList<>();
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://velapanti.esy.es")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        OtherApi requestInterface = retrofit.create(OtherApi.class);
        Call<List<QuoteList>> call = requestInterface.getJSON();
        call.enqueue(new Callback<List<QuoteList>>() {
            @Override
            public void onResponse(Call<List<QuoteList>> call, Response<List<QuoteList>> response) {
                qItems = response.body();
                if (isAdded()) {
                    otherAdapter = new QuoteAdapter(getActivity().getApplicationContext(), qItems);
                    otherRecycler.setAdapter(otherAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<QuoteList>> call, Throwable t) {

            }
        });
    }
}