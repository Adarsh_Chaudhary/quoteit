package com.adarsh.quoteit.fragment;

/**
 * Created by Adarsh on 11-08-2016.
 */
public class QuoteList {

    public int getId() {
        return id;
    }

    public String getQuote() {
        return quote;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    int id;
    String quote, name, email;
}
