package com.adarsh.quoteit;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.adarsh.quoteit.fragment.SlidingTabLayout;
import com.adarsh.quoteit.fragment.ViewPagerAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * Created by Adarsh on 29-09-2016.
 */
public class HomeActivity extends AppCompatActivity {
    ViewPager view_pager;
    ViewPagerAdapter viewAdapter;
    SlidingTabLayout tabs;
    CharSequence titles[] = {"Friendship", "Love", "Motivation", "Others"};
    int noTabs = 4;
    AdView ad_home;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ad_home = (AdView) findViewById(R.id.ad_home);
        AdRequest adRequest = new AdRequest.Builder().build();
        ad_home.loadAd(adRequest);

        view_pager = (ViewPager) findViewById(R.id.view_pager);
        viewAdapter = new ViewPagerAdapter(getSupportFragmentManager(), titles, noTabs);
        view_pager.setAdapter(viewAdapter);

        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(false);
        tabs.setViewPager(view_pager);

        if (!isConnection()) {
            showConnectionErrorDialog();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_sends:
                Intent launch_post = new Intent(HomeActivity.this, SendActivity.class);
                startActivity(launch_post);
                return true;
            case R.id.action_shares:
                Intent share = new Intent();
                share.setAction(Intent.ACTION_SEND);
                share.putExtra(Intent.EXTRA_TEXT, "Get " + getString(R.string.app_name) + " app to get interesting quotes with daily updation https://play.google.com/store/apps/details?id=" +getPackageName() + "\nDownload and share your favorite ones with world");
                share.setType("text/plain");
                startActivity(Intent.createChooser(share, "Choose an option..."));
                return true;
            case R.id.action_abouts:
                startActivity(new Intent(HomeActivity.this, AboutActivity.class));
                return true;
            case R.id.action_refresh:
                recreate();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showConnectionErrorDialog() {

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle("No Internet !!");
        builder.setMessage("Not connected to Internet Please turn it on and try again");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Handler h = new Handler();
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!isConnection()) {
                            showConnectionErrorDialog();
                        }
                    }
                }, 60000);
            }
        });
        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean isConnection() {
        ConnectivityManager manage = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manage.getActiveNetworkInfo();
        if (info != null && info.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
}